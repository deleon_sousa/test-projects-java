<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Primeira Página com SpringMVC</title>
<style type="text/css">
th, td {
    border: 1px solid black;
}
</style>
</head>
<body>
	<h4>Hello World Spring MVC!!!</h4>
	
	<table style="width:50%;"> 
		<tr>
			<th>Line 1</th>
			<th>Line 2</th>
		</tr>
		<tr>
			<td>cell 1</td>
			<td>cell 2</td>
		</tr>
	</table>
</body>
</html>