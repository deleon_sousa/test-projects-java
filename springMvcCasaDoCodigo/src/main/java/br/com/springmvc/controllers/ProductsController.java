package br.com.springmvc.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.springmvc.daos.ProductDAO;
import br.com.springmvc.model.BookType;
import br.com.springmvc.model.Product;

@Controller
@Transactional
public class ProductsController {

	@Autowired
	private ProductDAO productDAO;
	
	@RequestMapping("/produtos/form")
	public ModelAndView form() {
		ModelAndView modelAndView = new ModelAndView("products/produtos");
		modelAndView.addObject("types", BookType.values());
		return modelAndView;
	}

	@RequestMapping(value="/produtos",method=RequestMethod.POST)
	public String save(Product product) {
		System.out.println("Cadastrando o produto: " + product);
		productDAO.save(product);
		return "products/ok";
	}

	@RequestMapping(value="/produtos",method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("products/list");
		modelAndView.addObject("products", productDAO.list());
		return modelAndView;
	}
}
