package br.com.jsf.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItem;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.primefaces.component.selectmanymenu.SelectManyMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;


@ViewScoped
@ManagedBean
public class Cadastro {
	
	private String 	name  = "";
	private int 	idade;
	private String 	email  = "";
	private Date dtNascimento;
	private int valorCombo = 0;
	private List<String> selectedOptions;
	
	public Cadastro (){
		this.selectedOptions = new ArrayList<String>();
		this.selectedOptions.add("4");
	}
	
	public String salvar(){
		return null;
	}
	
	
//	@PreDestroy
//	public void teste(){
//		
//		String nome = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tab:nome");
//		System.out.println("----------DESTRUIDO_______________+++" + nome);
//	}
	
	public void changed(ValueChangeEvent e) throws Exception {
		
		
//		for (String str	: this.selectedOptions) {
//			str.equals("4");	
//			selectedOptions.remove(str);
//		}
		
		List<UIComponent> comList = e.getComponent().getChildren();
		
		for (UIComponent uiComponent : comList) {
			
			UISelectItem selectItem = (UISelectItem) uiComponent;
			
			String str = (String) selectItem.getItemValue();
			
			if(this.selectedOptions.contains(str))
				this.selectedOptions.remove(str);
			
			for (String str2 : this.selectedOptions) {
				System.out.println(str2);
			}
			
			selectItem.setRendered(false);
		}
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.update("cadastro:tab:basic");
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDtNascimento() {
		return dtNascimento;
	}
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public int getValorCombo() {
		return valorCombo;
	}
	public void setValorCombo(int valorCombo) {
		this.valorCombo = valorCombo;
	}
	public void onTabChange(TabChangeEvent event) {
        FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        String campo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hidden1");
        
        FacesMessage msg2 = new FacesMessage("Tab Changed", "Nome do Campo: " + campo);
        FacesContext.getCurrentInstance().addMessage(null, msg2);
    }
	public void addInfo(ActionEvent actionEvent) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Sample info message", "PrimeFaces rocks!"));
	}

	public void addWarn(ActionEvent actionEvent) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Sample warn message", "Watch out for PrimeFaces!"));
	}

	public void addError(ActionEvent actionEvent) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Sample error message", "PrimeFaces makes no mistakes"));
	}

	public void addFatal(ActionEvent actionEvent) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Sample fatal message", "Fatal Error in System"));
	}
	public List<String> getSelectedOptions() {
		return selectedOptions;
	}
	public void setSelectedOptions(List<String> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}
}
