package br.com.jsf.beans;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class Internacio {

	private Long valorMoeda;
	private Long valorMoeda2;
	private Long valorMoeda3;
	private Date data;


	@PostConstruct
	public void init(){
		valorMoeda = 658L;
		valorMoeda2 = 6585L;
		valorMoeda3 = 658669658L;
		data = new Date();
	}
	
	public Long getValorMoeda() {
		return valorMoeda;
	}

	public void setValorMoeda(Long valorMoeda) {
		this.valorMoeda = valorMoeda;
	}

	public Long getValorMoeda2() {
		return valorMoeda2;
	}

	public void setValorMoeda2(Long valorMoeda2) {
		this.valorMoeda2 = valorMoeda2;
	}

	public Long getValorMoeda3() {
		return valorMoeda3;
	}

	public void setValorMoeda3(Long valorMoeda3) {
		this.valorMoeda3 = valorMoeda3;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
