package br.com.jsf.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


@ManagedBean
@ViewScoped
public class TreeTable {
	
	public class Document implements Serializable{
		 
	    private String name;
	     
	    private String size;
	     
	    private String type;
	     
	    public Document(String name, String size, String type) {
	        this.name = name;
	        this.size = size;
	        this.type = type;
	    }

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSize() {
			return size;
		}

		public void setSize(String size) {
			this.size = size;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}

	private TreeNode root = new DefaultTreeNode();

	public void criarTreeTable(){
		this.criarPais();
	}
	
	private void criarPais(){
		
	//	this.root = new DefaultTreeNode(new Document("Files", "-", "Folder"), null);
		
		TreeNode documents = new DefaultTreeNode(new Document("Documents", "-", "Folder"), root);
		criarFilhosDocuments(documents);
	    TreeNode pictures = new DefaultTreeNode(new Document("Pictures", "-", "Folder"), root);
	    criarFilhosPictuers(pictures);
	    TreeNode movies = new DefaultTreeNode(new Document("Movies", "-", "Folder"), root);
	    criarFilhosMovies(movies);
	}
	
	private void criarFilhosDocuments(TreeNode pai){
		
		TreeNode work = new DefaultTreeNode(new Document("Work", "-", "Folder"), pai);
	    TreeNode primefaces = new DefaultTreeNode(new Document("PrimeFaces", "-", "Folder"), pai);
	}
		
	private void criarFilhosPictuers(TreeNode pai){
			
		TreeNode barca = new DefaultTreeNode("picture", new Document("barcelona.jpg", "30 KB", "JPEG Image"), pai);
	    TreeNode primelogo = new DefaultTreeNode("picture", new Document("logo.jpg", "45 KB", "JPEG Image"), pai);
	    TreeNode optimus = new DefaultTreeNode("picture", new Document("optimusprime.png", "96 KB", "PNG Image"), pai);	
			
	}
	
	private void criarFilhosMovies(TreeNode pai){
		
		TreeNode pacino = new DefaultTreeNode(new Document("Al Pacino", "-", "Folder"), pai);
		TreeNode deniro = new DefaultTreeNode(new Document("Robert De Niro", "-", "Folder"), pai);
	}
	
	public TreeNode getRoot() {
		if(root == null)
			root = new DefaultTreeNode();
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}
}
