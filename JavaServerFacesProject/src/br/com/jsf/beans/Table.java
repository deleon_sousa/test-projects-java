package br.com.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ViewScoped
@ManagedBean
public class Table implements Serializable {

	private static final long serialVersionUID = 1L;

	private final static String[] colors;
	
	private final static String[] strings;

	private final static String[] manufacturers;
	
	public final static String TESTE_FLASH = "TESTE_FLASH";

	static {
		colors = new String[10];
		colors[0] = "Black";
		colors[1] = "White";
		colors[2] = "Green";
		colors[3] = "Red";
		colors[4] = "Blue";
		colors[5] = "Orange";
		colors[6] = "Silver";
		colors[7] = "Yellow";
		colors[8] = "Brown";
		colors[9] = "Maroon";

		manufacturers = new String[10];
		manufacturers[0] = "Mercedes";
		manufacturers[1] = "BMW";
		manufacturers[2] = "Volvo";
		manufacturers[3] = "Audi";
		manufacturers[4] = "Renault";
		manufacturers[5] = "Opel";
		manufacturers[6] = "Volkswagen";
		manufacturers[7] = "Chrysler";
		manufacturers[8] = "Ferrari";
		manufacturers[9] = "Ford";
		
		strings = new String[8];
		strings[0] = "String 1";
		strings[1] = "String 2";
		strings[2] = "String 3";
		strings[3] = "String 4";
		strings[4] = "String 5";
		strings[5] = "String 6";
		strings[6] = "String 7";
		strings[7] = "String 8";
	}

	private List<Car> carsSmall;

	public Table() {
		carsSmall = new ArrayList<Car>();
		populateRandomCars(carsSmall, 9);
	}

	private void populateRandomCars(List<Car> list, int size) {
		
		List<String> lista = Arrays.asList(strings);
		
		for (int i = 0; i < size; i++)
			
			list.add(new Car(getRandomModel(), getRandomYear(),
					getRandomManufacturer(), getRandomColor(), lista));
	}
	
	public String navegar(){
		
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		externalContext.getFlash().put("bean", "Teste dessa porra que não funcionou na primeira");
		
		String param = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("testeParam");
		System.out.println("------------------------------------------------------"+param+"------------------------------------------------------");
		
		return "cadastro";
	}

	public List<Car> getCarsSmall() {
		return carsSmall;
	}

	private int getRandomYear() {
		return (int) (Math.random() * 50 + 1960);
	}

	private String getRandomColor() {
		return colors[(int) (Math.random() * 10)];
	}

	private String getRandomManufacturer() {
		return manufacturers[(int) (Math.random() * 10)];
	}

	private String getRandomModel() {
		return UUID.randomUUID().toString().substring(0, 8);
	}

	public static String[] getStrings() {
		return strings;
	}

	public static String getTESTE_FLASH() {
		return TESTE_FLASH;
	}
}
