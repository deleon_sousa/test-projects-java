package br.com.jsf.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import br.com.jsf.beans.TreeTable.Document;
import br.com.jsf.entidades.Humano;

@ManagedBean
@ViewScoped
public class TreeTableComEntidadeComplexa {
	
	private TreeNode root = new DefaultTreeNode();
	
	public void criarTreeTable(){
		this.criarPais();
	}
	
	private void criarPais(){
		
		TreeNode homem = new DefaultTreeNode(new Humano("Homem"), root);
		criarFilhos(homem);
		
		TreeNode mulher = new DefaultTreeNode(new Humano("Mulher"), root);
		criarFilhas(mulher);
	}
	
	private void criarFilhos(TreeNode pai){
		
		new DefaultTreeNode("homem", new Humano("Deleon", 26, 36), pai);
		new DefaultTreeNode("homem", new Humano("Pedro", 04, 23), pai);
	}
	
	private void criarFilhas(TreeNode mae){
		
		new DefaultTreeNode("homem", new Humano("Maria", 26, 58), mae);
		new DefaultTreeNode("homem", new Humano("Bruna", 14, 69), mae);
		new DefaultTreeNode("homem", new Humano("Karina", 24, 57), mae);
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}
}
