package br.com.jsf.entidades;

public class Humano {
	
	private String nome;
	private Integer idade;
	private Integer peso;
	private Endereco endereco;
	
	public Humano(String nome, Integer idade, Integer peso) {
		super();
		
		this.nome = nome;
		this.idade = idade;
		this.peso = peso;
		this.endereco = new Endereco("Rua a", 0001, "Bairro B");
	}
	
	public Humano(String nome) {
		super();
		
		this.nome = nome;
	}
	
	
	
	public Humano() {
		// TODO Auto-generated constructor stub
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public Integer getPeso() {
		return peso;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
