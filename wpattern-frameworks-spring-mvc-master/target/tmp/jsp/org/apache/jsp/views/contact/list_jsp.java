package org.apache.jsp.views.contact;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class list_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_form_form_method_action;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_form_form_method_action = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_form_form_method_action.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>WPattern Spring MVC</title>\n");
      out.write("<style>\n");
      out.write("/*\n");
      out.write("  project: CSS - table design\n");
      out.write("  type: stylesheet\n");
      out.write("  description: golden style\n");
      out.write("  edited: 14.09.2005, Michael Horn\n");
      out.write("*/\n");
      out.write("table {\n");
      out.write("  border-collapse: collapse;\n");
      out.write("  border: 2px solid #996;\n");
      out.write("  font: normal 80%/140% verdana, arial, helvetica, sans-serif;\n");
      out.write("  color: #333;\n");
      out.write("  background: #fffff0;\n");
      out.write("  }\n");
      out.write("caption {\n");
      out.write("  padding: 0 .4em .4em;\n");
      out.write("  text-align: left;\n");
      out.write("  font-size: 1em;\n");
      out.write("  font-weight: bold;\n");
      out.write("  text-transform: uppercase;\n");
      out.write("  color: #333;\n");
      out.write("  background: transparent;\n");
      out.write("  }\n");
      out.write("td, th {\n");
      out.write("  border: 1px solid #cc9;\n");
      out.write("  padding: .3em;\n");
      out.write("  }\n");
      out.write("thead th, tfoot th {\n");
      out.write("  border: 1px solid #cc9;\n");
      out.write("  text-align: left;\n");
      out.write("  font-size: 1em;\n");
      out.write("  font-weight: bold;\n");
      out.write("  color: #444;\n");
      out.write("  background: #dbd9c0;\n");
      out.write("  }\n");
      out.write("tbody td a {\n");
      out.write("  background: transparent;\n");
      out.write("  color: #72724c;\n");
      out.write("  text-decoration: none;\n");
      out.write("  border-bottom: 1px dotted #cc9;\n");
      out.write("  }\n");
      out.write("tbody td a:hover {\n");
      out.write("  background: transparent;\n");
      out.write("  color: #666;\n");
      out.write("  border-bottom: 1px dotted #72724c;\n");
      out.write("  }\n");
      out.write("tbody th a {\n");
      out.write("  background: transparent;\n");
      out.write("  color: #72724c;\n");
      out.write("  text-decoration: none;\n");
      out.write("  font-weight:bold;\n");
      out.write("  border-bottom: 1px dotted #cc9;\n");
      out.write("  }\n");
      out.write("tbody th a:hover {\n");
      out.write("  background: transparent;\n");
      out.write("  color: #666;\n");
      out.write("  border-bottom: 1px dotted #72724c;\n");
      out.write("  }\n");
      out.write("tbody th, tbody td {\n");
      out.write("  vertical-align: top;\n");
      out.write("  text-align: left;\n");
      out.write("  }\n");
      out.write("tfoot td {\n");
      out.write("  border: 1px solid #996;\n");
      out.write("  }\n");
      out.write(".odd {\n");
      out.write("  color: #333;\n");
      out.write("  background: #f7f5dc;\n");
      out.write("  }\n");
      out.write("tbody tr:hover {\n");
      out.write("  color: #333;\n");
      out.write("  background: #fff;\n");
      out.write("  }\n");
      out.write("tbody tr:hover th,\n");
      out.write("tbody tr.odd:hover th {\n");
      out.write("  color: #333;\n");
      out.write("  background: #ddd59b;\n");
      out.write("  }\n");
      out.write("\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\t<h2>Contacts</h2>\n");
      out.write("\t\n");
      out.write("\t<table >\n");
      out.write("\t\t<tr>\n");
      out.write("\t\t\t<th>Id</th>\n");
      out.write("\t\t\t<th>First Name</th>\n");
      out.write("\t\t\t<th>Last name</th>\n");
      out.write("\t\t\t<th>Email</th>\n");
      out.write("\t\t\t<th>Phone</th>\n");
      out.write("\t\t\t<th></th>\n");
      out.write("\t\t\t<th></th>\n");
      out.write("\t\t</tr>\n");
      out.write("\t\t");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t</table>\n");
      out.write("\t\n");
      out.write("\t<br/>\n");
      out.write("\t\n");
      out.write("\t");
      if (_jspx_meth_form_form_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\n");
      out.write("\t");
      if (_jspx_meth_form_form_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contactForm.contacts}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("contact");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t<tr>\n");
          out.write("\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.firstname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.lastname}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\t\t\t\t<td>");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.phone}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</td>\n");
          out.write("\t\t\t\t<td> <a href=\"/contact/edit.html?id=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Edit</a> </td>\n");
          out.write("\t\t\t\t<td> <a href=\"/contact/delete.html?id=");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${contact.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">Delete</a> </td>\n");
          out.write("\t\t\t</tr>\n");
          out.write("\t\t");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_form_form_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  form:form
    org.springframework.web.servlet.tags.form.FormTag _jspx_th_form_form_0 = (org.springframework.web.servlet.tags.form.FormTag) _jspx_tagPool_form_form_method_action.get(org.springframework.web.servlet.tags.form.FormTag.class);
    _jspx_th_form_form_0.setPageContext(_jspx_page_context);
    _jspx_th_form_form_0.setParent(null);
    _jspx_th_form_form_0.setAction("/contact/add.html");
    _jspx_th_form_form_0.setMethod("get");
    int[] _jspx_push_body_count_form_form_0 = new int[] { 0 };
    try {
      int _jspx_eval_form_form_0 = _jspx_th_form_form_0.doStartTag();
      if (_jspx_eval_form_form_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t<input type=\"submit\" value=\"Add\" />\n");
          out.write("\t");
          int evalDoAfterBody = _jspx_th_form_form_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_form_form_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_form_form_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_form_form_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_form_form_0.doFinally();
      _jspx_tagPool_form_form_method_action.reuse(_jspx_th_form_form_0);
    }
    return false;
  }

  private boolean _jspx_meth_form_form_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  form:form
    org.springframework.web.servlet.tags.form.FormTag _jspx_th_form_form_1 = (org.springframework.web.servlet.tags.form.FormTag) _jspx_tagPool_form_form_method_action.get(org.springframework.web.servlet.tags.form.FormTag.class);
    _jspx_th_form_form_1.setPageContext(_jspx_page_context);
    _jspx_th_form_form_1.setParent(null);
    _jspx_th_form_form_1.setAction("/contact/listEdit.html");
    _jspx_th_form_form_1.setMethod("get");
    int[] _jspx_push_body_count_form_form_1 = new int[] { 0 };
    try {
      int _jspx_eval_form_form_1 = _jspx_th_form_form_1.doStartTag();
      if (_jspx_eval_form_form_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t<input type=\"submit\" value=\"List Edit\" />\n");
          out.write("\t");
          int evalDoAfterBody = _jspx_th_form_form_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_form_form_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_form_form_1[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_form_form_1.doCatch(_jspx_exception);
    } finally {
      _jspx_th_form_form_1.doFinally();
      _jspx_tagPool_form_form_method_action.reuse(_jspx_th_form_form_1);
    }
    return false;
  }
}
