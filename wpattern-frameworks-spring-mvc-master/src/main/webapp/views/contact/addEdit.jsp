<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>WPattern Spring MVC</title>
</head>
<body>
	<c:choose>
		<c:when test="${contact.id == null}"><h2>Add Contact</h2></c:when>
		<c:when test="${contact.id != null}"><h2>Edit Contact</h2></c:when>
	</c:choose>
	
	<form:form method="post" action="/contact/save.html" modelAttribute="contact">
	
		<table>
			<input name="id" value="${contact.id}" hidden="true"  />
			<tr>
			<td><label>First Name: </label></td>
			<td> <input name="firstname" value="${contact.firstname}" /> <br/></td>
			</tr>
			
			<tr>
			<td><label>Last Name: </label></td>
			<td><input name="lastname" value="${contact.lastname}" /> <br/></td>
			</tr>
			<tr>
			<td><label>Email: </label></td>
			<td><input name="email" value="${contact.email}" /> <br/></td>
			</tr>
			
			<tr>
			<td><label>Phone: </label></td>
			<td><input name="phone" value="${contact.phone}" /> <br/></td>
			</tr>
		</table>
		<br/>
		<table>
			<tr>
			<td><input type="submit" name="action" value="Save" /></td>
			<td><input type="submit" name="action" value="Cancel" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
