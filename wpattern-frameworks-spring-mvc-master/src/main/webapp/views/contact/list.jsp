<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>WPattern Spring MVC</title>
<style>
/*
  project: CSS - table design
  type: stylesheet
  description: golden style
  edited: 14.09.2005, Michael Horn
*/
table {
  border-collapse: collapse;
  border: 2px solid #996;
  font: normal 80%/140% verdana, arial, helvetica, sans-serif;
  color: #333;
  background: #fffff0;
  }
caption {
  padding: 0 .4em .4em;
  text-align: left;
  font-size: 1em;
  font-weight: bold;
  text-transform: uppercase;
  color: #333;
  background: transparent;
  }
td, th {
  border: 1px solid #cc9;
  padding: .3em;
  }
thead th, tfoot th {
  border: 1px solid #cc9;
  text-align: left;
  font-size: 1em;
  font-weight: bold;
  color: #444;
  background: #dbd9c0;
  }
tbody td a {
  background: transparent;
  color: #72724c;
  text-decoration: none;
  border-bottom: 1px dotted #cc9;
  }
tbody td a:hover {
  background: transparent;
  color: #666;
  border-bottom: 1px dotted #72724c;
  }
tbody th a {
  background: transparent;
  color: #72724c;
  text-decoration: none;
  font-weight:bold;
  border-bottom: 1px dotted #cc9;
  }
tbody th a:hover {
  background: transparent;
  color: #666;
  border-bottom: 1px dotted #72724c;
  }
tbody th, tbody td {
  vertical-align: top;
  text-align: left;
  }
tfoot td {
  border: 1px solid #996;
  }
.odd {
  color: #333;
  background: #f7f5dc;
  }
tbody tr:hover {
  color: #333;
  background: #fff;
  }
tbody tr:hover th,
tbody tr.odd:hover th {
  color: #333;
  background: #ddd59b;
  }

</style>
</head>
<body>
	<h2>Contacts</h2>
	
	<table >
		<tr>
			<th>Id</th>
			<th>First Name</th>
			<th>Last name</th>
			<th>Email</th>
			<th>Phone</th>
			<th></th>
			<th></th>
		</tr>
		<c:forEach items="${contactForm.contacts}" var="contact">
			<tr>
				<td>${contact.id}</td>
				<td>${contact.firstname}</td>
				<td>${contact.lastname}</td>
				<td>${contact.email}</td>
				<td>${contact.phone}</td>
				<td> <a href="/contact/edit.html?id=${contact.id}">Edit</a> </td>
				<td> <a href="/contact/delete.html?id=${contact.id}">Delete</a> </td>
			</tr>
		</c:forEach>
	</table>
	
	<br/>
	
	<form:form action="/contact/add.html" method="get">
		<input type="submit" value="Add" />
	</form:form>
	
	<form:form action="/contact/listEdit.html" method="get">
		<input type="submit" value="List Edit" />
	</form:form>
</body>
</html>